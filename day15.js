var input = "0,1,4,13,15,12,16".split(",").map(i => BigInt(i));

var _cycle = list => ((list.slice(0, -1).includes(list[list.length-1]))
                     ? BigInt(list.slice(0, -1).reverse().indexOf(list[list.length-1])) + 1n
                     : 0n);
var cycle = list => list.concat([_cycle(list)]);
var repeat_n = n => list => [list].concat([..."a".repeat(n - list.length)].map((i,n)=>n)).reduce(
    (mylist, n) => cycle(mylist)
);
console.log(repeat_n(2020)(input)[2019]);

// Trying to FP part 2 in JS takes over 8 hours.
var indices = Object.assign({}, ...input.map((i, n) => ({[i]: BigInt(n)})))
var index = BigInt(input.length - 1);
var current = input[index];
var last_current;
while (index < 29999999n) {
    if (!(index % 100000n)) {
        console.log("Tick", index);
    }
    last_current = current;
    if (typeof indices[current] != "undefined") {
        current = index - indices[current];
    } else {
        current = 0n;
    }
    indices[last_current] = index;
    index += 1n;
}
console.log(current);



