' -*- mode: smalltalk; coding: utf-8 -*- '!
Object subclass: #DayEighteen
    instanceVariableNames: ''
    classVariableNames: ''
    poolDictionaries: ''
    category: nil.
!
!DayEighteen methodsFor: 'AoC'!
partOne
    | counter inFile |
    
    counter := 0.
    inFile := FileStream open: 'day18_in.txt' mode: FileStream read.
    inFile linesDo: [ :c | counter := counter + (Behavior evaluate: c) ].
    Transcript nextPutAll: counter asString ; nl.
! !
(DayEighteen new) partOne.




