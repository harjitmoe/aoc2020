#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

import re
from day17 import Line, LifeLike

grid = Line(Line(False))
opre = re.compile("([ns]?[ew])")

class HexWalker:
    def __init__(self, grid):
        self.grid = grid
        self.zero()
    def zero(self):
        self.x = self.y = 0
    # HexLife grid:
    # W   NW
    # SW  X   NE
    #     SE  E
    def walk(self, direct):
        self.__dict__.update({
            "w": {"x": self.x - 1, "y": self.y - 1},
            "nw": {"x": self.x, "y": self.y - 1},
            "sw": {"x": self.x - 1, "y": self.y},
            "ne": {"x": self.x + 1, "y": self.y},
            "se": {"x": self.x, "y": self.y + 1},
            "e": {"x": self.x + 1, "y": self.y + 1},
        }[direct])
    def flip(self):
        self.grid[self.x][self.y] = not self.grid[self.x][self.y]
    @property
    def population(self):
        return self.grid.count
    def consume_line(self, line):
        self.zero()
        for i in opre.split(line):
            if i:
                self.walk(i)
        self.flip()

class HexOne2dLifeLike(LifeLike):
    def __init__(self, grid, s=(1, 2), b=(2,)):
        super().__init__(2, s, b)
        self.grid = grid
    def neighbourhood(self, x, y):
        out = []
        # HexLife grid:
        # W   NW
        # SW  X   NE
        #     SE  E
        out.append(self.grid.soft_get(x-1, y-1))
        out.append(self.grid.soft_get(x, y-1))
        out.append(self.grid.soft_get(x-1, y))
        out.append(self.grid.soft_get(x+1, y))
        out.append(self.grid.soft_get(x, y+1))
        out.append(self.grid.soft_get(x+1, y+1))
        return out

# Part 1
walker = HexWalker(grid)
with open("day24_in.txt", "r") as f:
    for i in f:
        walker.consume_line(i.strip())
print(walker.population)

# Part 2
ca = HexOne2dLifeLike(grid, (1, 2), (2,))
for i in range(100):
    ca.cull()
    ca.print()
    print("Generation", i, "to", i+1)
    print()
    ca.step()
ca.print()
print(ca.grid.count)


