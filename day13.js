var input = `1006726
23,x,x,x,x,x,x,x,x,x,x,x,x,41,x,x,x,x,x,x,x,x,x,647,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,13,19,x,x,x,x,x,x,x,x,x,29,x,557,x,x,x,x,x,37,x,x,x,x,x,x,x,x,x,x,17`;

var initno = input => input.split("\n")[0] | 0;
var buses = input => input.split("\n")[1].split(",");
var busnos = input => buses(input).filter(i => i != "x").map(i => i | 0);
var key_to_cmp = keyer => (a, b) => (keyer(a) > keyer(b)) - (keyer(a) < keyer(b));
var dist = init => i => (i - (init % i)) % i;
var closest_busnos = input => init => busnos(input).slice().sort(key_to_cmp(dist(init)));
var closest_busno = input => init => closest_busnos(input)(init)[0];
var _part_one = init => id => id * dist(init)(id);
var part_one = input => init => _part_one(init)(closest_busno(input)(init));
console.log(part_one(input)(initno(input)));

var zip_buses = input => buses(input).map((i, n) => [n, i]).filter(i => i[1] != "x").map(
    i => [i[0], i[1]|0]);
var zip_buses_modulo = input => zip_buses(input).map(i => [i[0]%i[1], i[1]]);

var times = input => buses(input).map((i, n) => n).map(time => [time, 
    zip_buses_modulo(input).filter(bus => !((time - bus[0]) % bus[1])).map(i => i[1])
]).filter(i => i[1].length).sort(key_to_cmp(i => -i[1].length));
var _crucial_times = acc => time => unclaimed_periods => (
    [acc[0].concat(unclaimed_periods)].concat(acc.slice(1)).concat([[time, unclaimed_periods]])
);
var crucial_times = input => [[[]]].concat(times(input)).reduce((acc, i) =>
    _crucial_times(acc)(i[0])(i[1].filter(j => !(acc[0].includes(j))))
).slice(1).filter(i => i[1].length);
var composites = input => crucial_times(input).map(i => [i[0], i[1].reduce((a, b) => a * b)]);

// Can't FP this bit due to stack limits.
var multiplier = 1;
var compos = composites(input);
while (1) {
    var current = (compos[0][1] * multiplier) - compos[0][0];
    var this_one = true;
    for (var i of compos.slice(1)) {
        if ((current + i[0]) % i[1]) {
            multiplier += 1;
            this_one = false;
            break;
        }
    }
    if (this_one) {
        console.log(current);
        break;
    }
}





