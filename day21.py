import functools, collections

crypts = set()
encrypts = {}
sure_encrypts = {}
clocks = collections.defaultdict(int)

def procline(crypt, english):
    for i in crypt:
        clocks[i] += 1
    for i in english:
        myset = set(crypt)
        crypts.update(myset)
        encrypts.setdefault(i, myset).intersection_update(myset)

with open("day21_in.txt") as f:
    for i in f:
        crypt, english = i.rstrip(")\n").split(" (contains ", 1)
        crypt = crypt.split()
        english = english.split(", ")
        procline(crypt, english)

changed = True
while changed:
    changed = False
    grabs = {}
    for i in encrypts:
        if (len(encrypts[i]) == 1) and (i not in sure_encrypts):
            changed = True
            sure_encrypts[i] = [*encrypts[i]][0]
            grabs.setdefault(sure_encrypts[i], []).append(i)
        else:
            for j in sure_encrypts:
                if j in encrypts[i]:
                    changed = True
                    encrypts[i] ^= {j}
            for j in encrypts[i]:
                grabs.setdefault(j, []).append(i)
    for i in encrypts: # Sweep two
        for j in encrypts[i]:
            if (len(grabs[j]) == 1) and (len(encrypts[i]) != 1):
                # i.e. only i claims j, but i claims other stuff which is clearly wrong.
                changed = True
                encrypts[i] = {j}
                sure_encrypts[i] = j

allergen_candidates = functools.reduce(lambda a, b: a | b, encrypts.values())
print(len(crypts), len(allergen_candidates))
print(len(encrypts), len(sure_encrypts), sure_encrypts)
print("Part 1:", sum(clocks[i] for i in (crypts ^ allergen_candidates)))
print("Part 2:", ",".join(i[1] for i in sorted([*sure_encrypts.items()])))





