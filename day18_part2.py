#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-
import io, functools

def fooeval(f):
    "Eval an expr with addition, multiplication, single-digit positive integer literals "\
    "and brackets in which + is made to bind more tightly than *."
    multiplicands = []
    addends = []
    capturing = 0
    capture = []
    while i := f.read(1):
        if capturing:
            capture.append(i)
            if i == "(":
                capturing += 1
            elif i == ")":
                capturing -= 1
            #
            if capturing == 0:
                addends.append(fooeval(io.StringIO("".join(capture[:-1])))) # Cut the trailing `)`
                del capture[:]
        elif not i.strip():
            pass
        elif i in "0123456789": # they're all single digit
            addends.append(int(i, 10))
        elif i == "+":
            pass # Syntactically redundant since + binds tightest and literals are 1-digit
        elif i == "(":
            capturing += 1
        elif i == "*":
            multiplicands.append(sum(addends))
            del addends[:]
        else:
            raise ValueError
    if addends:
        multiplicands.append(sum(addends))
        del addends[:]
    return functools.reduce((lambda a, b: a * b), multiplicands)

accum = 0
for i in open("day18_in.txt", "r", encoding="utf-8"):
    accum += fooeval(io.StringIO(i))
print(accum)


