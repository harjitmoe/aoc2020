#!/usr/bin/env python3
# -*- mode: python; coding: utf-8 -*-

from ply import lex, yacc

tokens = ("AYY", "BEE")
t_AYY = "a"
t_BEE = "b"
t_ignore = "\n"
def t_error(t):
    print(f"Illegal character {t.value[0]!r}")
    t.lexer.skip(1)

lex.lex()

with open("day19_in.txt") as f:
    for i in f:
        if ":" not in i:
            continue
        u = "".join(chr(ord(j) + 49) if j.isdecimal() else j for j in i)
        u = u.replace(":", " :").replace('"a"', "AYY").replace('"b"', "BEE")
        mylabel = u.split(None, 1)[0]
        mylabel2 = i.split(None, 1)[0]
        assert mylabel != "error" # Would triger a false alarm if so
        u = u.replace("|", "\n" + (" " * len(mylabel)) + " |")
        print(u)
        def sho(mylabel, mylabel2):
            globals()[f"p_expression_{mylabel}"] = lambda p = mylabel2: \
                p.__setitem__(0, (mylabel2,) + tuple(p[1:]))
            globals()[f"p_expression_{mylabel}"].__doc__ = u
        sho(mylabel, mylabel2)

class NotValid(Exception): pass

def p_error(p):
    raise NotValid()

yacc.yacc()

matches = 0
fails = 0
with open("day19_in.txt") as f:
    for i in f:
        i = i.strip()
        if ":" in i:
            continue
        if not i:
            continue
        print(i, end=": ")
        try:
            assert yacc.parse(i)[0] == "0:"
        except NotValid:
            print("not valid")
            fails += 1
        else:
            print("valid")
            matches += 1

print(f"Failed: {fails:d}; Matched: {matches:d}")


