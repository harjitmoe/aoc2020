#!/usr/bin/env python3
# -*- mode: python; coding: utf-8 -*-

dispatch = {}

class NotValid(Exception): pass

def mungify(line):
    label, thing = line.split(": ", 1)
    stuffs = [i.split() for i in thing.split(" | ")]
    def mungified(inline, index_in):
        for option in stuffs:
            index = index_in
            try:
                for name in option:
                    if name[0] == '"':
                        literal = name[1:-1]
                        if (index < len(inline)) and (inline[index] == literal):
                            index += 1
                        else:
                            raise NotValid()
                    else:
                        index = dispatch[name](inline, index)
            except NotValid:
                continue
            else:
                return index
        raise NotValid()
    mungified.__name__ = "parse_" + label
    mungified.__qualname__ = "<parse_" + label + ">"
    dispatch[label] = mungified

def validate(i, huh="0"):
    if dispatch[huh](i, 0) < len(i):
        raise NotValid

with open("day19_in.txt") as f:
    for i in f:
        if ":" not in i:
            continue
        mungify(i)

matches = 0
fails = 0
with open("day19_in.txt") as f:
    for i in f:
        i = i.strip()
        if ":" in i:
            continue
        if not i:
            continue
        #print(i, end=": ")
        try:
            validate(i)
        except NotValid:
            #print("not valid")
            fails += 1
        else:
            #print("valid")
            matches += 1

print(f"Part 1—Failed: {fails:d}; Matched: {matches:d}")

# Part 2
# Can't do it like this, because the output of mungify cannot partially backtrack a rule whose
# parser has already returned:
#   mungify("8: 42 | 42 8")
#   mungify("11: 42 31 | 42 11 31")
# Instead, roll rules 0, 8 and 11 into one custom parser (match n 42s where n >= 2 followed by
# m 31s where 0 < m < n):
def hello_from_the_outside(inline, index_in):
    indices = []
    index = index_in
    h2g2 = g2h2 = 0
    indices.append((index, h2g2, g2h2))
    while 1:
        try:
            assert (not h2g2) or (g2h2 < h2g2)
            if index >= len(inline) or (g2h2 and g2h2 == (h2g2 - 1)):
                if g2h2:
                    return index
                else:
                    raise NotValid
            elif not g2h2:
                try:
                    index = dispatch["42"](inline, index)
                    h2g2 += 1
                except NotValid:
                    if h2g2 >= 2:
                        index = dispatch["31"](inline, index)
                        g2h2 += 1
                    else:
                        # Must be at least two 42s
                        raise NotValid
            else:
                index = dispatch["31"](inline, index)
                g2h2 += 1
            indices.append((index, h2g2, g2h2))
        except NotValid:
            # Pop up to the stage before any 31s were done
            while indices[-1][2]:
                indices.pop()
            # Try rolling back individual 42s and switching to 31s
            while 1:
                # Roll back a 42
                indices.pop()
                # If we've rolled back beyond the mandatory state of at
                # least two 42s (plus initial state), don't match.
                if not indices[2:]:
                    raise NotValid
                # Try a 31
                try:
                    index, h2g2, g2h2 = indices[-1]
                    index = dispatch["31"](inline, index)
                    g2h2 += 1
                    indices.append((index, h2g2, g2h2))
                except NotValid:
                    continue
                else:
                    break
dispatch["boolide"] = hello_from_the_outside
matches = 0
fails = 0
with open("day19_in.txt") as f:
    for i in f:
        i = i.strip()
        if ":" in i:
            continue
        if not i:
            continue
        #print(i, end=": ")
        try:
            validate(i, "boolide")
        except NotValid:
            #print("not valid")
            fails += 1
        else:
            #print("valid")
            matches += 1

print(f"Part 2—Failed: {fails:d}; Matched: {matches:d}")



