#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

key_one = 15113849
key_two = 4206373
loops_one = None
loops_two = None

signet = 1
loops = 0
while (not loops_one) or (not loops_two):
    signet = (signet * 7) % 20201227
    loops += 1
    if signet == key_one:
        loops_one = loops
    elif signet == key_two:
        loops_two = loops

signet_one = signet_two = 1
for i in range(loops_two):
    signet_one = (signet_one * key_one) % 20201227
for i in range(loops_one):
    signet_two = (signet_two * key_two) % 20201227

assert signet_one == signet_two, (signet_one, signet_two)
print("Part 1:", signet_one)


