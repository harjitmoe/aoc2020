var player1_testin = `9
2
6
3
1`.split("\n").map(i => i | 0);

var player2_testin = `5
8
4
7
10`.split("\n").map(i => i | 0);

var player1_in = `43
21
2
20
36
31
32
37
38
26
48
47
17
16
42
12
45
19
23
14
50
44
29
34
1`.split("\n").map(i => i | 0);

var player2_in = `40
24
49
10
22
35
28
46
7
41
15
5
39
33
11
8
3
18
4
13
6
25
30
27
9`.split("\n").map(i => i | 0);

var nextgen = gen => player1 => player2 => ((player1[0] > player2[0])
    ? [gen + 1, player1.slice(1).concat([player1[0], player2[0]]), player2.slice(1)]
    : [gen + 1, player1.slice(1), player2.slice(1).concat([player2[0], player1[0]])]);
var _run = state => run(state[0])(state[1])(state[2]);
var run = gen => player1 => player2 => ((player1.length && player2.length)
    ? _run(nextgen(gen)(player1)(player2))
    : [gen, player1, player2]);
var revenumerate1 = list => list.slice().reverse().map((i, n) => [n + 1, i]);
var score = player => revenumerate1(player).map(i => i[0] * i[1]).reduce((a, b) => a + b, 0);
var _part1 = results => score(results[1]) || score(results[2]);
var part1 = player1 => player2 => _part1(run(0)(player1)(player2));
console.log(part1(player1_in)(player2_in));

var nextgen2 = gen => player1 => player2 => (
    (player1.length > player1[0] && player2.length > player2[0])
    ? winner2(gen)(player1)(player2)(player1.slice(1).slice(0, player1[0]))(player2.slice(1).slice(0, player2[0]))
    : nextgen(gen)(player1)(player2)
);
var _winner2 = gen => player1 => player2 => state => ((state[1].length)
    ? [gen + 1, player1.slice(1).concat([player1[0], player2[0]]), player2.slice(1)]
    : [gen + 1, player1.slice(1), player2.slice(1).concat([player2[0], player1[0]])]
);
var winner2 = gen => rp1 => rp2 => p1 => p2 => _winner2(gen)(rp1)(rp2)(run2(0)(p1)(p2)([]));
var _run2 = state => deja => run2(state[0])(state[1])(state[2])(deja);
var run2_ = gen => player1 => player2 => deja => ((player1.length && player2.length)
    ? _run2(nextgen2(gen)(player1)(player2))(deja.concat([JSON.stringify([player1, player2])]))
    : [gen, player1, player2]);
var run2 = gen => player1 => player2 => deja => ((deja.includes(JSON.stringify([player1, player2])))
    ? [gen, player1, [], deja]
    : run2_(gen)(player1)(player2)(deja)
);
var part2 = player1 => player2 => _part1(run2(0)(player1)(player2)([]));
console.log(part2(player1_in)(player2_in));




