import collections, functools

pool = []
lookups = collections.defaultdict(list)

class Tile:
    def __init__(self, name, data):
        self.name = name
        self.data = data
        self.left = tuple(i[0] for i in data)
        self.right = tuple(i[-1] for i in data)
        self.top = data[0]
        self.bottom = data[-1]
        self.L2 = self.wagyu(self.left)
        self.R2 = self.wagyu(self.right)
        self.T2 = self.wagyu(self.top)
        self.B2 = self.wagyu(self.bottom)
    def wagyu(self, fing):
        if fing[::-1] in lookups:
            if self not in lookups[fing[::-1]]:
                lookups[fing[::-1]].append(self)
            return fing[::-1]
        else:
            if self not in lookups[fing]:
                lookups[fing].append(self)
            return fing
    def rotate(self): # rotate 90deg ccw
        self.__init__(self.name, tuple(zip(*(i[::-1] for i in self.data))))
    def flip_lr(self):
        self.__init__(self.name, tuple(i[::-1] for i in self.data))
    def flip_ud(self):
        self.__init__(self.name, self.data[::-1])
    def shed(self):
        self.__init__(self.name, tuple(i[1:-1] for i in self.data[1:-1]))
    def find_monsters(self, pat):
        coords = []
        for y in range(0, len(self.data) - len(pat) + 1):
            for x in range(0, len(self.data[y]) - len(pat[0]) + 1):
                for dy, row in enumerate(pat):
                    for dx, criterion in enumerate(row):
                        if criterion == " ":
                            continue
                        elif criterion == "#" and self.data[y + dy][x + dx] == "#":
                            continue
                        else:
                            break
                    else:
                        continue
                    break
                else:
                    coords.append((x, y))
        return coords
    def mark_monsters(self, pat):
        data = [list(i) for i in self.data]
        for (x, y) in self.find_monsters(pat):
            for dy, row in enumerate(pat):
                for dx, criterion in enumerate(row):
                    if criterion == "#":
                        assert data[y + dy][x + dx] == "#"
                        data[y + dy][x + dx] = "O"
        self.__init__(self.name, tuple(tuple(i) for i in data))
    def count_hashes(self):
        hashes = 0
        for row in self.data:
            for char in row:
                if char == "#":
                    hashes += 1
        return hashes

def cat_lr(left, right):
    data = []
    assert len(left.data) == len(right.data)
    for lr, rr in zip(left.data, right.data):
        data.append(lr + rr)
    return Tile(None, data)

def cat_ud(up, down):
    return Tile(None, up.data + down.data)

with open("day20_in.txt") as f:
    intile = False
    tile = []
    name = None
    for i in f:
        if not intile:
            name = int(i.strip().split(None, 1)[1].rstrip(":"), 10)
            intile = True
        elif i.strip():
            tile.append(tuple(i.strip()))
        else:
            intile = False
            pool.append(Tile(name, tuple(tile)))
            del tile[:]

# Part 1
edges = [lookups[i][0] for i in lookups if len(lookups[i]) == 1]
corners = set(i for i in edges if edges.count(i) > 1)
assert len(corners) == 4
print(functools.reduce(lambda a, b: a * b.name, corners, 1))

# Part 2
initial = [*corners][0]
print("Rotating initial corner")
while not (len(lookups[initial.L2]) == 1 and len(lookups[initial.T2]) == 1):
    initial.rotate()
print("Filling top row")
top_row = [initial]
while len(lookups[top_row[-1].R2]) > 1:
    x = frozenset(lookups[top_row[-1].R2]) ^ frozenset({top_row[-1]})
    assert len(x) == 1
    x = [*x][0]
    while x.L2 != top_row[-1].R2:
        x.rotate()
    if x.left != top_row[-1].right:
        x.flip_ud()
    assert len(lookups[x.T2]) == 1, lookups[x.T2]
    top_row.append(x)
print("Pulling more rows")
rows = [tuple(top_row)]
while len(lookups[rows[-1][0].B2]) > 1:
    print("Row {:d}".format(len(rows) + 1))
    current_row = []
    while len(current_row) < len(top_row):
        above = rows[-1][len(current_row)]
        x = frozenset(lookups[above.B2]) ^ frozenset({above})
        assert len(x) == 1
        x = [*x][0]
        while x.T2 != above.B2:
            x.rotate()
        if x.top != above.bottom:
            x.flip_lr()
        if current_row:
            assert x.left == current_row[-1].right
        current_row.append(x)
    rows.append(tuple(current_row))
assert frozenset(functools.reduce(lambda a, b: a + b, rows)) == frozenset(pool)
[i.shed() for i in pool]
rrows = [functools.reduce(cat_lr, row) for row in rows]
whole = functools.reduce(cat_ud, rrows)

pat_monster = tuple(tuple(i) for i in """\
                  # 
#    ##    ##    ###
 #  #  #  #  #  #   """.split("\n"))

while 1:
    if whole.find_monsters(pat_monster):
        print("Found monsters")
        break
    elif whole.find_monsters(pat_monster[::-1]):
        print("Flipping")
        whole.flip_ud()
    else:
        print("Turning")
        whole.rotate()

whole.mark_monsters(pat_monster)
print(whole.count_hashes())
#print("\n".join("".join(i) for i in whole.data))



