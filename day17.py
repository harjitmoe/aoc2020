class Line(list):
    def __init__(self, default):
        super().__init__()
        self._default = default
        self.offset = 0
    @property
    def default(self):
        return self._default.deepcopy() if hasattr(self._default, "deepcopy") else self._default
    def _ensure(self, idx):
        ridx = idx - self.offset
        while ridx < 0:
            self.insert(0, self.default)
            self.offset -= 1
            ridx += 1
        while ridx >= len(self):
            self.append(self.default)
        return ridx
    def drop_left(self):
        self.pop(0)
        self.offset += 1
    def drop_right(self):
        self.pop()
    def soft_get(self, *idx):
        if len(idx) == 1:
            idx, = idx
        else:
            return self.soft_get(idx[0]).soft_get(*idx[1:])
        ridx = idx - self.offset
        if (ridx < 0) or (ridx >= len(self)):
            return self.default
        return super().__getitem__(ridx)
    def __getitem__(self, idx):
        if isinstance(idx, tuple):
            if len(idx) == 1:
                idx, = idx
            else:
                return self[idx[0]][idx[1:]]
        return super().__getitem__(self._ensure(idx))
    def __setitem__(self, idx, val):
        if isinstance(idx, tuple):
            if len(idx) == 1:
                idx, = idx
            else:
                self[idx[0]][idx[1:]] = val
                return
        return super().__setitem__(self._ensure(idx), val)
    def deepcopy(self):
        ret = Line(self.default)
        for i in self:
            if hasattr(i, "deepcopy"):
                ret.append(i.deepcopy())
            elif hasattr(i, "copy"):
                ret.append(i.copy())
            else:
                ret.append(i)
        ret.offset = self.offset
        return ret
    @property
    def lowest(self):
        return self.offset
    @property
    def highest(self):
        return self.offset + len(self) - 1
    @property
    def count(self):
        ret = 0
        for i in self:
            if isinstance(i, Line):
                ret += i.count
            else:
                ret += bool(i)
        return ret

class LifeLike:
    def __init__(self, dimensions=2, s=(2, 3), b=(3,)):
        self.s, self.b = s, b
        self.dim = dimensions
        self.grid = False
        for i in range(dimensions):
            self.grid = Line(self.grid)
    def neighbourhood(self, *coords):
        raise NotImplementedError("implement in subclass")
    def _get_lohi(self, _coords, _grid):
        if not _coords:
            return _grid.lowest, _grid.highest
        return (min(self._get_lohi(_coords[1:], j)[0] for j in _grid if j),
                max(self._get_lohi(_coords[1:], j)[1] for j in _grid if j))
    def _step(self, _coords=(), _grid=None, _presgrid=None):
        in_grid = _grid if _grid != None else self.grid # Only if None as opposed to []
        presgrid = _presgrid if _presgrid != None else self.grid.deepcopy()
        out_grid = in_grid.deepcopy()
        low, high = self._get_lohi(_coords, presgrid)
        #print(f"{_coords}: {low}, {high}")
        for i in range(low - 1, high + 2):
            if isinstance(in_grid[i], Line):
                ouk = self._step(_coords=_coords+(i,), _grid=in_grid[i], _presgrid=presgrid)
                out_grid[i] = ouk
                assert str(out_grid[i]) == str(ouk)
            else:
                neigh = self.neighbourhood(*_coords, i)
                count = neigh.count(True)
                #print("→", _coords+(i,), in_grid[i], count)
                if in_grid[i]:
                    if count in self.s:
                        out_grid[i] = True
                        assert out_grid[i]
                    else:
                        out_grid[i] = False
                        assert not out_grid[i]
                else:
                    if count in self.b:
                        out_grid[i] = True
                        assert out_grid[i]
                    else:
                        out_grid[i] = False
                        assert not out_grid[i]
                #print("←", _coords+(i,), out_grid[i], count)
        return out_grid
    def step(self):
        self.grid = self._step()
    def cull(self, grid = None):
        grid = grid if grid != None else self.grid
        if not len(grid):
            return
        elif isinstance(grid[grid.lowest], Line):
            while len(grid) and grid[grid.lowest].count == 0:
                grid.drop_left()
            while len(grid) and grid[grid.highest].count == 0:
                grid.drop_right()
            for i in grid:
                self.cull(i)
        else:
            while len(grid) and not grid[grid.lowest]:
                grid.drop_left()
            while len(grid) and not grid[grid.highest]:
                grid.drop_right()
    def populate(self, goltext):
        for y, row in enumerate(goltext.split("\n")):
            for x, char in enumerate(row):
                coords = (x, y) + ((0,) * (self.dim - 2))
                if char in "#*oO":
                    self.grid[coords] = True
                elif char in "b.":
                    self.grid[coords] = False
                else:
                    raise ValueError
    def print(self):
        if self.dim == 2:
            miny = min(i.lowest for i in self.grid)
            maxy = max(i.highest for i in self.grid)
            for y in range(miny, maxy + 1):
                for x in range(self.grid.lowest, self.grid.highest + 1):
                    print(end = "o" if self.grid[x][y] else ".")
                print()
            print()
        elif self.dim != 3:
            raise ValueError("can't print a non-2D non-3D CA")
        else:
            minz = min(min(j.lowest for j in i) for i in self.grid if len(i))
            maxz = max(max(j.highest for j in i) for i in self.grid if len(i))
            miny = min(i.lowest for i in self.grid)
            maxy = max(i.highest for i in self.grid)
            for z in range(minz, maxz + 1):
                for y in range(miny, maxy + 1):
                    for x in range(self.grid.lowest, self.grid.highest + 1):
                        print(end = "o" if self.grid[x][y][z] else ".")
                    print()
                print()
        

class MooreOneLifeLike(LifeLike):
    def neighbourhood(self, first_coord, *rest_coords, full_coords=None, test_coords=()):
        if not full_coords:
            full_coords = (first_coord,) + rest_coords
        out = []
        for i in [first_coord-1, first_coord, first_coord+1]:
            full_test_coords = test_coords + (i,)
            if rest_coords:
                out.extend(self.neighbourhood(*rest_coords, full_coords=full_coords,
                                              test_coords=full_test_coords))
            else:
                if full_coords != full_test_coords:
                    #print(full_coords, full_test_coords)
                    #out.append(self.grid.deepcopy()[full_test_coords])
                    out.append(self.grid.soft_get(*full_test_coords))
        return out

testinput = """.#.
..#
###"""

myinput = """.#.#.#..
..#....#
#####..#
#####..#
#####..#
###..#.#
#..##.##
#.#.####"""

butterfly = """O...
OO..
O.O.
.OOO"""

butterfly2 = """...o
..oo
.o.o
ooo."""

if __name__ == "__main__":
    # Part 1
    test = MooreOneLifeLike(3, (2, 3), (3,))
    test.populate(myinput)
    for i in range(6):
        test.cull()
        test.print()
        print("Generation", i, "to", i+1)
        print()
        test.step()
    test.print()
    print(test.grid.count)

    # Part 2
    test = MooreOneLifeLike(4, (2, 3), (3,))
    test.populate(myinput)
    for i in range(6):
        test.cull()
        print("Generation", i, "to", i+1)
        test.step()
    print(test.grid.count)





