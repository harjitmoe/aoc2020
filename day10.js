var input = `83
69
170
56
43
111
117
135
136
176
154
65
107
169
141
151
158
134
108
143
114
104
49
55
72
73
144
13
35
152
98
133
31
44
150
70
118
64
39
137
142
28
130
167
101
100
120
79
153
157
89
163
177
3
1
38
16
128
18
62
76
78
17
63
160
59
175
168
54
34
22
174
53
25
129
90
42
119
92
173
4
166
19
2
121
7
71
99
66
46
124
86
127
159
12
91
140
52
80
45
33
9
8
77
147
32
95`.split("\n").map(i => i | 0);

var _differentiate = seq => [[[null, 0]]].concat(seq).reduce(
                            (a, b) => a.concat([[b, b - a[a.length - 1][0]]]));
var differentiate = seq => _differentiate(seq).slice(2).map(i => i[1]);
var count = seq => item => seq.filter(i => i === item).length;
var sort_me = input => input.slice().sort((a, b) => a - b);

var _all_joltages = sorted => [0].concat(sorted.concat(sorted[sorted.length-1] + 3));
var all_joltages = input => _all_joltages(sort_me(input));
var joltage_steps = input => differentiate(all_joltages(input));
var _part_one = steps => count(steps)(1) * count(steps)(3);
var part_one = input => _part_one(joltage_steps(input));
console.log(JSON.stringify(all_joltages(input)));
console.log(part_one(input));

var next_steps = input => cur => sort_me(input.filter(i => i > cur && (i - cur) <= 3));
var count_possibilities = joltages => {
    var memo = {};
    var mypartial = cur => {
        if (typeof memo[cur] != "undefined") {
            return memo[cur];
        } else if (cur != joltages[joltages.length-1]) {
            return (memo[cur] = next_steps(joltages)(cur).map(mypartial).reduce((a, b) => a + b));
        } else {
            return (memo[cur] = 1);
        }
    };
    return mypartial;
};
console.log(count_possibilities(all_joltages(input))(0));










